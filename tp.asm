;*********************************************************************
; Alumno: Murck, Pablo Martin
; Padron: 96694 
;
; TP 19 - Intel - Manejo de Matrices (I)
;
; Matrices: M filas X N columnas
; Matriz: - 1 byte -> M filas (1-8)
;		  - 1 byte -> N columnas (1-8)
;		  - 8x8 bytes (contenido maximo)
;
;*********************************************************************

%define CRLF 13,10
%define DOSNULL "$"

%macro  print_msg 1 
		mov dx,%1
		mov ah,9
		int 21h
%endmacro
%macro  print_NEWLINE 0
		mov dx,msg_newline
		mov ah,9
		int 21h
%endmacro
%macro get_char 0
		mov	ah,1h
		int	21h
%endmacro
%macro get_char_silent 0
		mov	ah,8h
		int	21h
%endmacro
%macro put_char 1 ;OJO: recibe en dl, escribe al tambien
	mov  dl,%1
	mov  ah,2
	int  21h
%endmacro

; FIN Macros y defines

segment pila	stack
      	  resb 64
stacktop:

segment datos data

; Datos:

matriz	dw 0  ;M y N   Acumulador
		times 8 resb 8
		dw 0  ;M y N	Matriz 1
		times 8 resb 8
		dw 0  ;M y N	Matriz 2
		times 8 resb 8
		dw 0  ;M y N	Matriz 3
		times 8 resb 8
		dw 0  ;M y N	Matriz 4
		times 8 resb 8
		dw 0  ;M y N	Matriz 5
		times 8 resb 8

signo resb 1
nro   resb 1

; Mensajes:
; - generales:
msg_newline db " ",CRLF,DOSNULL
msg_matriz_no_inicializada db "Matriz no inicializada!",CRLF,DOSNULL
msg_bievenida db "TP 19 Intel - Manejo Matrices I",CRLF,DOSNULL

msg_seleccionar_matriz db "Seleccione una matriz (1-5): ",DOSNULL
msg_seleccionar_fila_m db "Defina la(s) fila(s) (1-"
max_m				   db "8): ",DOSNULL

msg_seleccionar_columna_n db "Defina la(s) columna(s) (1-"
max_n				   	  db "8): ",DOSNULL

msg_cargar_valor_en_pos db "Cargar valor en posicion "
fila_m db "1,"
columna_n db "1 : ",DOSNULL

msg_elija db "Elija una opcion: ",DOSNULL

; - El menu:
msg_menu db "MENU:                | 0: Salir del programa",CRLF,\
			"1: Cargar una matriz | 2: Imprimir una matriz",CRLF,\
			"3: Sumar matrices    | 4: Transponer matriz",CRLF,\
			"5: Modif/Consultar valor de matriz",CRLF,DOSNULL

; - En la suma:
msg_sumar_menu db "SUMAR:                        | 0: Finalizar suma",CRLF,\
				  "1: Seleccionar matriz a sumar | 2: Imprimir la suma actual",CRLF,\
				  "3: Guardar la suma en una matriz",CRLF,DOSNULL
msg_suma_imposible db "La suma no se puede efectuar: N y/o M no concuerdan",CRLF,DOSNULL
msg_suma_overflow  db "Overflow! Lo siguiente no se pudo sumar:",CRLF,"En la posicion ",
overflow_m		   db "X,"
overflow_n		   db "X los valores: "
overflow_valor_1   db "xxx y "
overflow_valor_2   db "xxx",CRLF,DOSNULL

; - En modificiacion de valor:
msg_pedir_modificacion db "Desea modificar este valor? (s/n): ",DOSNULL
msg_nuevo_valor	   db "Ingrese el nuevo valor: ",DOSNULL
					

segment codigo code

;*********************************************************************
; Menu principal
;*********************************************************************

..start:
	mov	ax,datos
	mov	ds,ax
	mov es,ax
	mov	ax,pila
	mov	ss,ax
	mov sp,stacktop
	print_msg msg_bievenida
menu:
	push menu			;para que hagan ret sin que yo haga call.
	print_NEWLINE
	print_msg msg_menu
	print_msg msg_elija
menu_elegir:
	get_char_silent
	cmp al,"0"
	jl  menu_elegir
	cmp al,"5"
	jg  menu_elegir
	;Selecciono correctamente, muestro el char
	put_char al
	print_NEWLINE
	je  modif_valor_matriz
	cmp al,"1"
	je  cargar_matriz
	cmp al,"2"
	je  imprimir_matriz
	cmp al,"3"
	je  sumar_matrices
	cmp al,"4"
	je  transponer_matriz
    ; al == "0", chequeado arriba -> Retorno al DOS
	mov	ax,4c00h
	int	21h
	
;*********************************************************************
; Procedimientos generales
;*********************************************************************

; Pasa un binario de 8bits con signo a ASCII (limitado a (-99,99))
; Recibo nro de 8 bits en AL
; Devuelve el nro: [signo] [nro],DH   (nro es el 2do digito)
;
int_to_ascii:
	cmp al,0
	jge .es_positivo
	; es negativo
	mov byte[signo],"-"
	neg al
	jmp .set_chars
.es_positivo:
	mov byte[signo],"+"
.set_chars:
	mov ah,0
	mov dl,10
	div dl
	add ah,"0"
	add al,"0"
	mov dh,ah
	mov [nro],al
	ret
	
; Recibe caracteres ASCII por teclado que convertira en un
;	BPF c/signo de 8bits. (Limitado a la ingresion de (-99,99))
; No recibe nada
; Devuelve el nro en AL
;
entrada_ascii_to_int:
	mov byte[signo],"+"
	mov byte[nro],0
	jmp get_signo_y_numero
check_signo:
	cmp byte[signo],"-"
	jne forzar_ret
	neg byte[nro]
forzar_ret:
	mov al,[nro]
	ret
	
get_signo_y_numero:
	get_char_silent
	cmp al,13		;Enter implica numero por defecto -> 0
	je	forzar_ret 	;Es 0 si o si -> hago ret porque termine
	cmp al,"+"
	je  .mostrar
	cmp al,"-"
	je  .es_negativo
	cmp al,"0"
	jl  get_signo_y_numero
	cmp al,"9"
	jg  get_signo_y_numero
	;es numero y es el primero (ya lo dejo en binario)
	mov [nro],al
	sub byte[nro],"0"
	jmp .mostrar
.es_negativo:
	mov [signo],al
.mostrar:
	put_char al
	
get_numero:
	get_char_silent
	cmp al,13		;Enter implica guardar numero solo escrito antes
	je	check_signo	;quizas termino con ingresion de signo y 1 digito 
	cmp al,"0"
	jl  get_numero
	cmp al,"9"
	jg  get_numero
	;es numero y lo muestro
	put_char al
	sub al,"0"
	cmp byte[nro],0	;si no es cero armo un nro de 2 digitos.
	jne multip
	mov [nro],al
	jmp get_numero	

multip: ;multiplicar X10 lo que estaba en nro
	xchg al,[nro]
	mov ah,10
	mul ah
	add [nro],al
	jmp check_signo ;fuerzo la finalizacion del ingreso.
	
; Pide la seleccion de una matriz y calcula su direccion.
; Devuelve la posicion de la matriz en BX.
;
seleccionar_matriz:
	print_msg msg_seleccionar_matriz
.otro:
	get_char_silent
	cmp al,"1"
	jl  .otro
	cmp al,"5"
	jg 	.otro
	put_char al
	print_NEWLINE
	sub	al,"0"
	mov ah,66
	mul ah			;ax = ah*al
	mov bx,matriz
	add bx,ax
	ret

; Pide la seleccion de una fila: desde 1 a [max_m] (inclusive)
;  max_m debe setearse antes con el nro maximo en char ASCII
; Devuelve el M seleccionado como BPF en AL.
;
seleccionar_fila_m:
	print_msg msg_seleccionar_fila_m
.otro:
	get_char_silent
	cmp al,"1"
	jl  .otro
	cmp al,[max_m]
	jg 	.otro
	put_char al
	print_NEWLINE
	sub	al,"0"
	ret

; Pide la seleccion de una columna: desde 1 a [max_n] (inclusive)
;  max_n debe setearse antes con el nro maximo en char ASCII
; Devuelve el N seleccionado como BPF en AL.
;	
seleccionar_columna_n:
	print_msg msg_seleccionar_columna_n
.otro:
	get_char_silent
	cmp al,"1"
	jl  .otro
	cmp al,[max_n]
	jg 	.otro
	put_char al
	print_NEWLINE
	sub	al,"0"
	ret
	
; Copia la matriz apuntada por SI a la apuntada por DI
;
copiar_matrices:
	mov ah,[si]
	mov al,[si+1]
	mul ah		;ax = bytes en uso en matriz (aparte de 1eros 2 bytes)
	add ax,2	;agrego los 2 primeros bytes de M y N
	mov cx,ax
	rep movsb
	ret
	
;*********************************************************************
; Carga de Matriz
;*********************************************************************

setear_m_filas:
	mov byte[max_m],"8"
	call seleccionar_fila_m
	mov [bx],al
	ret

setear_n_columnas:
	mov byte[max_n],"8"
	call seleccionar_columna_n
	mov [bx+1],al
	ret	
	
cargar_matriz:
	call seleccionar_matriz 	;deja puntero matriz en BX
	call setear_m_filas			;guarda el m en la matriz
	call setear_n_columnas  	;guarda el n en la matriz
	mov ax,[bx]					;no molesta el endiannes
	mul ah		;ax = bytes en uso en matriz (aparte de 1eros 2 bytes)
	mov cx,ax				;cx = ah*al = M*N = (1-64)
	mov byte[fila_m],"1"
	mov byte[columna_n],"1"
	mov di,2
.cargar:
	print_msg msg_cargar_valor_en_pos
	call entrada_ascii_to_int
	print_NEWLINE
	mov [bx+di],al
	inc di
	inc byte[columna_n]
	mov ah,48
	add ah,[bx+1]
	cmp [columna_n],ah
	jle .otro
	mov byte[columna_n],"1"
	inc byte[fila_m]
.otro:	
	loop .cargar
	ret

;*********************************************************************
; Imprimir una Matriz
;*********************************************************************

imprimir_matriz:
	call seleccionar_matriz
	cmp byte[bx],0
	je	.matriz_no_inicializada
	call print_matriz
	ret
	
.matriz_no_inicializada:
	print_msg msg_matriz_no_inicializada
	ret
	
; Muestra por pantalla la matriz apuntada por BX
;
print_matriz:
	mov ch,0
	mov cl,[bx]
	jcxz .ret
	mov di,2
.otra_fila:
	push cx
	mov cl,[bx+1]
.otra_columna:
	call print_valor
	inc di
	loop .otra_columna
	print_NEWLINE
	pop cx
	loop .otra_fila
.ret:
	ret
	
; Imprime el valor apuntado por BX+DI (byte)
; (limitado a (-99,+99))
;
print_valor:
	mov al,[bx+di]
	call int_to_ascii
	put_char [signo]
	put_char [nro]
	put_char dh
	put_char " " ;print un espacio
	ret

;*********************************************************************
; Suma de Matrices
;*********************************************************************
	
sumar_matrices:
	call seleccionar_matriz
	cmp byte[bx],0
	je	.matriz_no_inicializada
	mov si,bx
	mov di,matriz
	call copiar_matrices
.menu:
	print_NEWLINE
	print_msg msg_sumar_menu
	print_msg msg_elija
.elegir:
	get_char_silent
	cmp al,"0"
	jl  .elegir
	cmp al,"3"
	jg  .elegir
	;Selecciono correctamente, muestro el char
	put_char al
	print_NEWLINE
	je 	acum_a_matriz
	cmp al,"2"
	je  print_acum
	cmp al,"1"
	je  sumar_matriz
	ret ;al == 0, chequeado arriba
	
.matriz_no_inicializada:
	print_msg msg_matriz_no_inicializada
	ret
	
print_acum:
	mov bx,matriz
	call print_matriz
	jmp sumar_matrices.menu
	
acum_a_matriz:
	mov si,matriz
	call seleccionar_matriz
	mov di,bx
	call copiar_matrices
	jmp sumar_matrices.menu
	
sumar_matriz:
	call seleccionar_matriz
	mov ax,[matriz]			;comparo directo la Word de M,N 
	cmp ax,[bx]				;El Endiannes no molesta!
	jne .imposible_sumar
	mov ch,0
	mov cl,[bx]
	mov di,2
.otra_fila:
	push cx
	mov cl,[bx+1]
.otra_columna:
	;copio y extiendo los valores a 16 bits (sosteniendo el signo)
	mov al,[bx+di]
	cmp al,0
	jl	.set_ah_negativo
	mov ah,0
	jmp .set_dx
.set_ah_negativo:
	mov ah,-1
.set_dx:
	mov dl,[matriz+di]
	cmp dl,0
	jl 	.set_dh_negativo
	mov dh,0
	jmp .sumar
.set_dh_negativo:
	mov dh,-1
.sumar:
	add ax,dx
	;chequeo overflow de +-99
	cmp ax,99
	jg	.overflow
	cmp ax,-99
	jl  .overflow
	;la suma paso los chequeos -> copio
	mov [matriz+di],al
	inc di
	loop .otra_columna
	pop cx
	loop .otra_fila
	jmp sumar_matrices.menu
	
.imposible_sumar:
	print_msg msg_suma_imposible
	jmp sumar_matrices.menu
	
.overflow: ;OJO: cx tiene columna, tope pila es fila (ambos invertidos ej: 8->1)
	mov al,[bx+1]
	inc al
	sub al,cl
	call int_to_ascii
	mov [overflow_n],dh
	pop cx
	mov al,[bx]
	inc al
	sub al,cl
	call int_to_ascii
	mov [overflow_m],dh
	mov al,[matriz+di]
	call int_to_ascii
	mov al,[signo]
	mov [overflow_valor_1],al
	mov al,[nro]
	mov [overflow_valor_1+1],al
	mov [overflow_valor_1+2],dh
	mov al,[bx+di]
	call int_to_ascii
	mov al,[signo]
	mov [overflow_valor_2],al
	mov al,[nro]
	mov [overflow_valor_2+1],al
	mov [overflow_valor_2+2],dh
	print_msg msg_suma_overflow
	ret ;VOLVER A MENU PRINCIPAL
	
;*********************************************************************
; Transponer una Matriz
;*********************************************************************
	
transponer_matriz:
	call seleccionar_matriz
	cmp byte[bx],0
	je	.matriz_no_inicializada
	call transponer_matriz_a_acum
	mov bx,matriz
	call print_matriz
	ret
	
.matriz_no_inicializada:
	print_msg msg_matriz_no_inicializada
	ret
	
; Transpone la matriz apuntada por BX dejandola en el acumulador.
;
transponer_matriz_a_acum:
	mov ah,[bx]
	mov [matriz+1],ah
	inc bx
	mov al,[bx]		;AH = M filas, AL = N Columnas
	mov [matriz],al
	inc bx
	mov ch,0
	mov cl,al
	mov si,2
.otra_columna:
	push cx
	mov cl,ah
	mov di,0 ;saltos de filas
.otra_fila:
	mov dl,[bx+di]
	mov [matriz+si],dl
	inc si
	push ax
	mov ah,0
	add ax,di
	mov di,ax
	pop ax
	loop .otra_fila
	inc bx
	pop cx
	loop .otra_columna
	ret
	
;*********************************************************************
; Modif/Consultar valor de Matriz
;*********************************************************************
	
modif_valor_matriz:			;(Mpedido - 1)*N + (Npedido-1)
	call seleccionar_matriz
	cmp byte[bx],0
	je	.matriz_no_inicializada
	mov ax,[bx]				;Trata como word, invierte -> al = M ,ah = N
	add ah,"0"
	add al,"0"
	mov [max_m],al
	mov [max_n],ah
	call seleccionar_fila_m
	dec al
	mov ch,al						;ch = Mpedido - 1
	call seleccionar_columna_n		
	dec al							;al = Npedido - 1
	mov cl,al						;cl = "
	mov ah,0
	mov al,[bx+1]					;al = N columnas
	mul ch							;ax = (al*ch) = (Mpedido - 1)*N
	add al,cl						;ax debe estar entre (0-63)
	add ax,2
	mov di,ax
	call print_valor
	print_NEWLINE
	call pedir_modificacion
	print_NEWLINE
	ret
	
.matriz_no_inicializada:
	print_msg msg_matriz_no_inicializada
	ret
	
pedir_modificacion:
	print_msg msg_pedir_modificacion
.elegir:
	get_char_silent
	cmp al,"n"
	je  .return
	cmp al,"s"
	je  .modificar_valor
	jmp .elegir
	
.modificar_valor:
	put_char al
	print_NEWLINE
	print_msg msg_nuevo_valor
	call entrada_ascii_to_int
	mov [bx+di],al
	ret
	
.return:
	put_char al
	ret
